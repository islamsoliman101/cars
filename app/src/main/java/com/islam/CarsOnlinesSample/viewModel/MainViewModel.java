package com.islam.CarsOnlinesSample.viewModel;

import android.databinding.ObservableField;
import android.util.Log;
import android.view.View;

import com.islam.CarsOnlinesSample.model.data.RetrofitHelper;
import com.islam.CarsOnlinesSample.model.entity.Car;
import com.islam.CarsOnlinesSample.view.CarsGridAdapter;
import com.islam.CarsOnlinesSample.view.CarsListAdapter;
import com.islam.CarsOnlinesSample.view.CompletedListener;

import rx.Subscriber;

/**
 * Created by Islam Soliman on 2018/7/10.
 */
public class MainViewModel {
    public ObservableField<Integer> contentViewVisibility;
    public ObservableField<Integer> progressBarVisibility;
    public ObservableField<Integer> errorInfoLayoutVisibility;
    public ObservableField<String> exception;
    private Subscriber<Car> subscriber;
    private CarsListAdapter carsListAdapter;
    private CarsGridAdapter carsGridAdapter;

    private CompletedListener completedListener;

    public MainViewModel(CarsListAdapter carsListAdapter, CarsGridAdapter carsGridAdapter, CompletedListener completedListener) {
        this.carsListAdapter = carsListAdapter;
        this.carsGridAdapter = carsGridAdapter;
        this.completedListener = completedListener;
        initData();
        getCarsOnline();
    }

    private void getCarsOnline() {
        subscriber = new Subscriber<Car>() {
            @Override
            public void onCompleted() {
                Log.d("[MainViewModel]", "onCompleted");
                hideAll();
                contentViewVisibility.set(View.VISIBLE);
                completedListener.onCompleted();
            }

            @Override
            public void onError(Throwable e) {
                hideAll();
                errorInfoLayoutVisibility.set(View.VISIBLE);
                exception.set(e.getMessage());
            }

            @Override
            public void onNext(Car car) {
                if (carsGridAdapter == null)
                    carsListAdapter.addItem(car);
                else
                    carsGridAdapter.addItem(car);
            }
        };
        RetrofitHelper.getInstance().getCars(subscriber);
    }

    public void refreshData() {
        getCarsOnline();
    }

    private void initData() {
        contentViewVisibility = new ObservableField<>();
        progressBarVisibility = new ObservableField<>();
        errorInfoLayoutVisibility = new ObservableField<>();
        exception = new ObservableField<>();
        contentViewVisibility.set(View.GONE);
        errorInfoLayoutVisibility.set(View.GONE);
        progressBarVisibility.set(View.VISIBLE);
    }

    private void hideAll() {
        contentViewVisibility.set(View.GONE);
        errorInfoLayoutVisibility.set(View.GONE);
        progressBarVisibility.set(View.GONE);
    }
}
