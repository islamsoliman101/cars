package com.islam.CarsOnlinesSample.viewModel;

import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.os.CountDownTimer;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.islam.CarsOnlinesSample.R;
import com.islam.CarsOnlinesSample.model.entity.Car;
import com.islam.CarsOnlinesSample.util.App;
import com.islam.CarsOnlinesSample.util.Language;

/**
 * Created by Islam Soliman on 2018/7/10.
 */

public class CarViewModel extends BaseObservable {
    private Car car;
    public ObservableField<String> timer = new ObservableField<>("");
    public ObservableField<Boolean> textColor = new ObservableField<>(false);
    private long millisUntilFinish;
    private Language language = new Language();

    public CarViewModel(Car car) {
        this.car = car;
        millisUntilFinish = this.car.getAuctionInfo().getEndDate() * 1000;
        countDown();
    }

    public String getName() {
        if (language.LoadLanguage(App.getContext()).equals(Language.lang_en))
            return car.getMakeEn() + " " + car.getModelEn() + " " + car.getYear();
        else
            return car.getMakeAr() + " " + car.getModelAr() + " " + car.getYear();

    }

    public String getPrice() {
        if (language.LoadLanguage(App.getContext()).equals(Language.lang_en))
            return car.getAuctionInfo().getCurrentPrice() + car.getAuctionInfo().getCurrencyEn();
        else
            return car.getAuctionInfo().getCurrentPrice() + car.getAuctionInfo().getCurrencyAr();

    }

    public String getLot() {
        return String.valueOf(car.getAuctionInfo().getLot());
    }

    public String getBids() {
        return String.valueOf(car.getAuctionInfo().getBids());
    }

    public String getTime(long millisUntilFinished) {
        long seconds = millisUntilFinished / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;
        String time = days + ":" + hours % 24 + ":" + minutes % 60 + ":" + seconds % 60;
        if (minutes < 5)
            textColor.set(true);
        else
            textColor.set(false);
        return time;
    }

    public void countDown() {

        new CountDownTimer(millisUntilFinish, 1000) {

            public void onTick(long millisUntilFinished) {
                millisUntilFinish = millisUntilFinished;
                car.getAuctionInfo().setEndDate((int) (millisUntilFinish / 1000));
                timer.set(getTime(millisUntilFinished));
            }

            public void onFinish() {
                timer.set("0:00:00:0");
            }
        }.start();
    }

    public String getImageUrl() {
        String url = car.getImage();
        url = url.replace("[h]", "100");
        url = url.replace("[w]", "130");
        return url;
    }

    @BindingAdapter({"app:imageUrl"})
    public static void loadImage(ImageView imageView, String url) {
        Glide.with(imageView.getContext())
                .load(url)
                .placeholder(R.drawable.car_default)
                .error(R.drawable.car_default)
                .into(imageView);

    }

}
