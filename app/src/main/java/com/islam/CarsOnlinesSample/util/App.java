package com.islam.CarsOnlinesSample.util;

import android.app.Application;
import android.content.Context;

/**
 * Created by Fady on 7/11/18.
 */

public class App extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();

    }


    public static Context getContext() {
        return context;
    }
}
