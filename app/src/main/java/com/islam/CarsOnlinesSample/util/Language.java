package com.islam.CarsOnlinesSample.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;

import java.util.Locale;

public class Language {
    private static String language = "en_US";
    public static String lang_ar = "ar";
    public static String lang_en = "en";
    private static final String Language = "Lang";
    private Context context;
    static String DATABASE_NAME = "cars";
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public void setCurrentLocal(Context c) {
        Locale locale = new Locale(LoadLanguage(c));
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        c.getResources().updateConfiguration(config, null);
    }


    public void SaveLanguage(Context context, String value) {
        sharedPreferences = context.getSharedPreferences(DATABASE_NAME,
                Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString(Language, value);
        editor.commit();
    }

    public String LoadLanguage(Context context) {
        sharedPreferences = context.getSharedPreferences(DATABASE_NAME,
                Context.MODE_PRIVATE);
        String strSavedMem1 = sharedPreferences.getString(Language, "en");
        return strSavedMem1;

    }


}
