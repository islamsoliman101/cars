
package com.islam.CarsOnlinesSample.model.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;
/**
 * Created by Islam Soliman on 2018/7/10.
 */
public class CarsResponse<T> {

    private String alertEn;
    private String alertAr;
    @SerializedName("Error")
    private Error error;
    @SerializedName("RefreshInterval")
    private Integer refreshInterval;
    @SerializedName("Ticks")
    private String ticks;
    private Integer count;
    private Integer endDate;
    private String sortOption;
    private String sortDirection;
    @SerializedName("Cars")
    private List<Car> cars = null;

    public String getAlertEn() {
        return alertEn;
    }

    public void setAlertEn(String alertEn) {
        this.alertEn = alertEn;
    }

    public String getAlertAr() {
        return alertAr;
    }

    public void setAlertAr(String alertAr) {
        this.alertAr = alertAr;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Integer getRefreshInterval() {
        return refreshInterval;
    }

    public void setRefreshInterval(Integer refreshInterval) {
        this.refreshInterval = refreshInterval;
    }

    public String getTicks() {
        return ticks;
    }

    public void setTicks(String ticks) {
        this.ticks = ticks;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getEndDate() {
        return endDate;
    }

    public void setEndDate(Integer endDate) {
        this.endDate = endDate;
    }

    public String getSortOption() {
        return sortOption;
    }

    public void setSortOption(String sortOption) {
        this.sortOption = sortOption;
    }

    public String getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(String sortDirection) {
        this.sortDirection = sortDirection;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

}
