package com.islam.CarsOnlinesSample.model.data;

import com.islam.CarsOnlinesSample.model.entity.Car;
import com.islam.CarsOnlinesSample.model.entity.CarsResponse;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by Islam Soliman on 2018/7/10.
 */
public interface CarsOnlineService {
    String BASE_URL = "http://api.emiratesauction.com/v2/";

    @GET("carsonline")
    Observable<CarsResponse<List<Car>>> getCarsOnline();
}
