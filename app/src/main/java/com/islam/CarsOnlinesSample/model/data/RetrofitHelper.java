package com.islam.CarsOnlinesSample.model.data;

import com.islam.CarsOnlinesSample.model.entity.Car;
import com.islam.CarsOnlinesSample.model.entity.CarsResponse;

import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Islam Soliman on 2018/7/10.
 */
public class RetrofitHelper {
    private static final int DEFAULT_TIMEOUT = 10;
    private Retrofit retrofit;
    private CarsOnlineService carsService;
    OkHttpClient.Builder builder;

    /**
     * RetrofitHelper
     * */
    private static class Singleton {
        private static final RetrofitHelper INSTANCE = new RetrofitHelper();
    }

    public static RetrofitHelper getInstance() {
        return Singleton.INSTANCE;
    }

    private RetrofitHelper() {
        builder = new OkHttpClient.Builder();
        builder.connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS);

        retrofit = new Retrofit.Builder()
                .client(builder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(CarsOnlineService.BASE_URL)
                .build();
        carsService = retrofit.create(CarsOnlineService.class);
    }

    public void getCars(Subscriber<Car> subscriber) {
        carsService.getCarsOnline()
                .map(new Func1<CarsResponse<List<Car>>, List<Car>>() {
                    @Override
                    public List<Car> call(CarsResponse<List<Car>> listResponse) {
                        return listResponse.getCars();
                    }
                })
                .flatMap(new Func1<List<Car>, Observable<Car>>() {
                    @Override
                    public Observable<Car> call(List<Car> cars) {
                        return Observable.from(cars);
                    }
                })

                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
}
