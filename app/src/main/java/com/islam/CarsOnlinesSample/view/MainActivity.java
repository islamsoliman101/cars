package com.islam.CarsOnlinesSample.view;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.islam.CarsOnlinesSample.R;
import com.islam.CarsOnlinesSample.util.Language;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Islam Soliman on 2018/7/10.
 */
public class MainActivity extends AppCompatActivity {
    @BindView(R.id.langBtn)
    ImageView LanguageView;
    private Language language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        language = new Language();
        ButterKnife.bind(this);
        transaction(0);
        setLanguageImage();
    }


    @OnClick(R.id.backArrow)
    void back() {
        finish();
    }

    @OnClick(R.id.btnFilter)
    void btnFilter() {
        // TODo filter Button Click
    }

    @OnClick(R.id.btnGrid)
    void btnGrid() {
        transaction(1);
    }

    @OnClick(R.id.btnSort)
    void btnSort() {
        transaction(0);
    }

    @OnClick(R.id.langBtn)
    void langBtn() {

        if (language.LoadLanguage(this).equalsIgnoreCase("ar")) {
            changeLanguage(Language.lang_en, R.drawable.en_language);
        } else {
            changeLanguage(Language.lang_ar, R.drawable.ar_language);
        }
        restartActivity();
    }

    private void changeLanguage(String lang_en, int en_language) {
        language.SaveLanguage(this, lang_en);
        language.setCurrentLocal(this);
        LanguageView.setBackgroundResource(en_language);
    }

    private void restartActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    // type  0 for Sort and 1 for grid
    private void transaction(int type) {
        FragmentManager fm = getFragmentManager();
        Bundle arguments = new Bundle();
        arguments.putInt("Type", type);
        CarFragment myFragment = CarFragment.getInstance();
        myFragment.setArguments(arguments);
        fm.beginTransaction().replace(R.id.car_fragment, myFragment).commit();
    }


    private void setLanguageImage() {
        if (language.LoadLanguage(this).equalsIgnoreCase("ar")) {
            LanguageView.setImageResource(R.drawable.en_language);
        } else {
            LanguageView.setImageResource(R.drawable.ar_language);
        }
    }
}
