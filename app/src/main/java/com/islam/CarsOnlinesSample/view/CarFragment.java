package com.islam.CarsOnlinesSample.view;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.islam.CarsOnlinesSample.R;
import com.islam.CarsOnlinesSample.databinding.CarsFragmentBinding;
import com.islam.CarsOnlinesSample.viewModel.MainViewModel;

/**
 * Created by Islam Soliman on 2018/7/10.
 */
public class CarFragment extends Fragment implements CompletedListener, SwipeRefreshLayout.OnRefreshListener {

    private static String TAG = CarFragment.class.getSimpleName();
    private MainViewModel viewModel;
    private CarsFragmentBinding carsFragmentBinding;
    private CarsListAdapter carsListAdapter;
    private CarsGridAdapter carsGridAdapter;

    private int typeOFView;

    public static CarFragment getInstance() {
        return new CarFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.cars_fragment, container, false);
        carsFragmentBinding = CarsFragmentBinding.bind(contentView);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            typeOFView = bundle.getInt("Type", 0);
        }
        initData();
        return contentView;
    }

    private void initData() {
        carsListAdapter = new CarsListAdapter();
        carsGridAdapter = new CarsGridAdapter();
        // type of view detect if it list or grid
        // 0 for list
        //1 for grid
        if (typeOFView == 0) {
            carsFragmentBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
            carsFragmentBinding.recyclerView.setItemAnimator(new DefaultItemAnimator());
            carsFragmentBinding.recyclerView.setAdapter(carsListAdapter);
            viewModel = new MainViewModel(carsListAdapter,null, this);

        } else {
            carsFragmentBinding.recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            carsFragmentBinding.recyclerView.setItemAnimator(new DefaultItemAnimator());
            carsFragmentBinding.recyclerView.setAdapter(carsGridAdapter);
            viewModel = new MainViewModel(null,carsGridAdapter, this);

        }
        carsFragmentBinding.swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorPrimary, R.color.colorPrimaryDark);
        carsFragmentBinding.swipeRefreshLayout.setOnRefreshListener(this);
        carsFragmentBinding.setViewModel(viewModel);

    }

    @Override
    public void onRefresh() {
        carsListAdapter.clearItems();
        carsGridAdapter.clearItems();
        viewModel.refreshData();
    }

    @Override
    public void onCompleted() {
        if (carsFragmentBinding.swipeRefreshLayout.isRefreshing()) {
            carsFragmentBinding.swipeRefreshLayout.setRefreshing(false);
        }
    }
}
