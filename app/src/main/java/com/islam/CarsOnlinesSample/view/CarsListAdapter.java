package com.islam.CarsOnlinesSample.view;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.islam.CarsOnlinesSample.R;
import com.islam.CarsOnlinesSample.databinding.CarItemListBinding;
import com.islam.CarsOnlinesSample.model.entity.Car;
import com.islam.CarsOnlinesSample.viewModel.CarViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Islam Soliman on 2018/7/10.
 */
public class CarsListAdapter extends RecyclerView.Adapter<CarsListAdapter.BindingHolder> {
    private List<Car> cars;

    public CarsListAdapter() {
        cars = new ArrayList<>();
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        CarItemListBinding itemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.car_item_list, parent, false);
        return new BindingHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        CarViewModel carViewModel = new CarViewModel(cars.get(position));
        holder.itemBinding.setViewModel(carViewModel);
    }

    @Override
    public int getItemCount() {
        return cars.size();
    }

    public void addItem(Car car) {
        cars.add(car);
        notifyItemInserted(cars.size() - 1);
    }

    public void clearItems() {
        cars.clear();
        notifyDataSetChanged();
    }

    public static class BindingHolder extends RecyclerView.ViewHolder {
        private CarItemListBinding itemBinding;

        public BindingHolder(CarItemListBinding itemBinding) {
            super(itemBinding.cardView);
            this.itemBinding = itemBinding;
        }
    }
}
