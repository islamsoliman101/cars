package com.islam.CarsOnlinesSample.view;

/**
 * Created by Islam Soliman on 2018/7/10.
 */
public interface CompletedListener {
    void onCompleted();
}
